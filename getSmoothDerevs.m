function [ n_knee,alpha ] = getSmoothDerevs( ang , dt , samples)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
SampleRate=1/dt;
n_kneeRAW=diff(ang)/dt*(1/6); %rpm
[B,A]=butter(4,10/(SampleRate/2));  %(cut off freq)/((Sample rate)/2)
n_knee=filtfilt(B,A,n_kneeRAW);
n_knee(samples)=interp1(1:samples-1,n_knee,samples,'linear','extrap'); % adding last sample
alpha=-diff(n_knee)/dt*(pi/30); % [rad/sec^2]
alpha(samples)=interp1(1:samples-1,alpha,samples,'linear','extrap'); % adding last sample

end

