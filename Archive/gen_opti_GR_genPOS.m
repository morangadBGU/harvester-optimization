clear all; 
close all;
set(0,'DefaultFigureWindowStyle','docked');
expData=0;
GRs=243; GRe=243;
ii=12; %motor for test
f=1; %defines the efficancy function
clutchPower=1;
clutchWeight=200;
HarvestPR=0.15;
gear_eff=1; %gear_eff=0.65; 
ElectEff=0.9;
max_torque=15;%[Nm]
Ig_supp=0.248;%[gcm^2]
R_wire=0;
weight_supp=1500-125;%weight of one system without motor [gr]
if f==3 weight_supp=weight_supp+clutchWeight; end

K1=5:1:13; %negative work cells of K1
K3=57:1:73; %negative work cells of K3
K4=84:1:98; %negative work cells of K4
POS=[1:4,14:56,74:83,99:100]; %positive work cells
samples=101;
%TODO Replce data to NCSU data
% alldata=xlsread('knee_data.xls','sheet1');
load('winterData.mat');
Tk=alldata(1:samples,3);% [Nm]
if(expData)
    load('expData.mat');
    Ts=5000; st=3.612e4; en=4.148e4;
    T=(en-st)/Ts;    
%     ang=FiltAngs(st:round((en-st)/samples):en); ang=ang(1:samples);
% TODO: first dervitaive the interp1
    ang=(interp1(st:en,FiltAngs(st:en),linspace(st,en,samples)))';
    t=linspace(0,T,samples); %sec
    dt=(t(end)-t(1))/length(t);
    n_knee=diff(ang)/dt*(1/6); %rpm    
    n_knee(samples)=interp1(1:samples-1,n_knee,samples,'linear','extrap'); % adding last sample 
    alpha=-diff(n_knee)/dt*(pi/30); % [rad/sec^2]
    alpha(samples)=interp1(1:samples-1,alpha,samples,'linear','extrap'); % adding last sample 
    p=-n_knee*(pi/30).*Tk; % [Watt]  
else    
    t=alldata(1:samples,7);
    dt=(t(end)-t(1))/length(t);
    ang=alldata(1:samples,2);% [deg]
    n_knee=alldata(1:samples,4);% [rpm]
    alpha=-alldata(1:samples,5);% [rad/sec^2]
    p=alldata(1:samples,6);% [Watt]        
end
    

% motors Data: 

Rg=([0.413 0.978 2.77 7.41 0.367 0.807 0.248 0.524 0.323 1.24 13.5 0.527 2.3 1.22]+R_wire)./(3^0.5); % Rg - motor resistence [oham]
%Rg(ii)=0;
Km=[25.1 33.5 47.5 101 16.7 31.5 8.95 16.9 10.5 21.1 66 14 28.1 27.1]./(3^0.5); % Km - torque constant [mNm/A]
Kn=[380 285 201 95 572 303 1070 565 907 453 145 680 340 352].*(3^0.5); % Kn - sapeed constant [rpm/V]
Ig=([135 135 135 135 24.2 24.2 10.5 10.5 5.54 5.54 5.54 5.54 5.54 8.91]+Ig_supp); % Iner- motor inertia [g*cm2]
%Ig(ii)=0;
% Ec=[1 1 1 1 1 1 1 1 1 1 1 1 1 1];
weight=2*([110 110 110 110 240 240 170 170 125 125 125 125 125 170]+weight_supp)*1e-3; %[Kg]
V=4.68; W=80; %human's weight
hip=50; %hip length
x=20; %device C.M above knee
m=weight(ii); %Total of 2 Devices weight
% weight_lost=W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - (x*(W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - W*(2*exp((103*V)/500 + (59*m)/2000 + 59/100) - 2*exp((103*V)/500 + 59/100))))/hip;
weight_lost=W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - (x*(W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - W*(exp((103*V)/500 + (59*m)/2000 + 59/100) - exp((103*V)/500 + 59/100))))/hip;
%Eliran interpolated eqaution
weight_lost_E=weight_lost*t(end);
% weight_lost_E=6; %from Moran's exp

width=[12.8 12.8 12.8 12.8 36 36 26 26 48.5 48.5 48.5 48.5 48.5 48.5]*1e-3;
diameter=[45 45 45 45 40 40 40 40 22 22 22 22 22 22]*1e-3;
motor_no=[339285 251601 339286 339287 339243 339244 339241 313320 323217 323219 327739 323218 323220 311538];



%Resistance range
Rl_minp=0.1;  
Rl_maxp=35; 
Rl_stepp=0.1; 



P_max_arr=zeros(samples,1);
Rl_best_arr=zeros(samples,1);
% GenAndInertia_best_arr=zeros(samples,1);
GenT_best_arr=zeros(samples,1);
InertiaT=zeros(samples,1);
wGen=n_knee;

Pi=0;
total_effGR=-1000;
C_const=0.3107/243;

P_max_arrGR=zeros(samples,1);
Rl_best_arrGR=zeros(samples,1);
GenAndInertia_best_arrGR=zeros(samples,1);
GenT_best_arrGR=zeros(samples,1);
InertiaTGR=zeros(samples,1);
inertia_wasteGR=zeros(samples,1);
total_eff_arr=zeros(GRe-GRs+1,1);

for GR=GRs:GRe
%     gear_eff=(-6.5896*log(GR)+94.292)/100; % maxon dc gear efficency in absulote value
    InertiaT=(Ig(ii)*(10^(-7))*GR^2)*alpha(1:end); % [Nm]     
    FrictionT=(C_const*GR*(pi/30))*n_knee;
    inertia_waste=trapz((pi/30)*n_knee(POS).*InertiaT(POS))*dt/gear_eff;
    GenAndInertia_best_arr=FrictionT-InertiaT;
    if f==1
        human_effort=(2*4*inertia_waste+weight_lost_E);
    end
    if (f==2)||(f==3)        
        human_effort=(weight_lost_E);%(ii));
    end           
            for n=[K1,K3,K4]                   
                Pi_max=0; Rl_best=0; GenT_best=0; GenAndInertia_best=0;                                                                                                
                for Rl= Rl_minp:Rl_stepp: Rl_maxp; % choosing the load resistence                                        
                    GenT=(n_knee(n)*Km(ii))/(1000*Kn(ii)*(Rl+Rg(ii)))*GR^2;                    
                    GenAndInertia=(GenT-InertiaT(n)+FrictionT(n));
%                     KneeT=(Tk(n))*HarvestPR*gear_eff;                
%                     if((abs(GenAndInertia)<abs(KneeT))&&(abs(GenAndInertia)<max_torque))
                    if((abs(GenAndInertia)/HarvestPR<abs(Tk(n)))&&(abs(GenAndInertia)<max_torque))
                        Pi=((n_knee(n)*GR)/(Kn(ii)*(Rl+Rg(ii))))^2*Rl;
                        
                        if (Pi>Pi_max)
                            Pi_max=Pi;
                            Rl_best=Rl*2/3; %R phase to R load
                            GenAndInertia_best=GenAndInertia;
                            GenT_best=GenT;                        
                        end
                    end
                end
                if(Pi_max>0) % there was a solutions in constrains
                    P_max_arr(n)=Pi_max;
                    Rl_best_arr(n)=Rl_best;   
                    GenAndInertia_best_arr(n)=GenAndInertia_best;
                    GenT_best_arr(n)=GenT_best;
                end
%                 if(n==84)
%                     disp('stop');
%                 end
            end
                        
            E_tot=2*(trapz(P_max_arr)*dt);         
            if (f==1)
                total_eff=E_tot/(human_effort);                         
            end
            if (f==2)
                total_eff=(E_tot-(2*inertia_waste))/(2*human_effort);   
            end
            if (f==3)
                total_eff=(E_tot-(2*clutchPower))/(2*human_effort);   
            end            
            
            total_eff_arr(1+GR-GRs)=total_eff;
            if total_eff>total_effGR
                total_effGR=total_eff;
                P_max_arrGR=P_max_arr*ElectEff;
                inertia_wasteGR=(2)*inertia_waste;
                InertiaTGR=InertiaT;
                Rl_best_arrGR=Rl_best_arr;
                GenAndInertia_best_arrGR=GenAndInertia_best_arr;
                GenT_best_arrGR=GenT_best_arr;
                %P_max_arrGR(POS)=InertiaT(POS).*n_knee(POS)/gear_eff;
                GRopt=GR;
                human_effortGR=human_effort;
                E_totGR=E_tot;
            end
end
            
            
%             hold off;
        figure()
        plot(p,'LineWidth',2)
        hold on;
        
        plot(-P_max_arrGR,'g','LineWidth',2);
        
%         plot(1:4,-P_max_arrGR(1:4),'--r','LineWidth',2);
%         plot(14:56,-P_max_arrGR(14:56),'--r','LineWidth',2);
%         plot(74:82,-P_max_arrGR(74:82),'--r','LineWidth',2);
%         plot(99:100,-P_max_arrGR(99:100),'--r','LineWidth',2);
                
        hold on;
        line([K1(1) K1(1)],[-50 50],'color','r','LineWidth',2);
        line([K1(end) K1(end)],[-50 50],'color','r','LineWidth',2);
        line([K3(1) K3(1)],[-50 50],'color','r','LineWidth',2);
        line([K3(end) K3(end)],[-50 50],'color','r','LineWidth',2);
        line([K4(1) K4(1)],[-50 50],'color','r','LineWidth',2);
        line([K4(end) K4(end)],[-50 50],'color','r','LineWidth',2);
        grid on;        
        legend('knee Power','Harvest Power');
        ylabel('[Watt]');
        xlabel('sample no.');
        
        figure()
        plot(Tk,'LineWidth',2)
        hold on;        
        plot(GenAndInertia_best_arrGR,'k','LineWidth',2);
        hold on;        
%         load('tmp.mat');
%         plot(a,'--k','LineWidth',2);
%         hold on;        
        plot(GenT_best_arrGR,'--g','LineWidth',2);
        hold on;        
        plot(InertiaTGR,'--r','LineWidth',2);
        hold on;
        plot(FrictionT,'--c','LineWidth',2);
        hold on;
        line([K1(1) K1(1)],[-50 50],'color','r','LineWidth',2);
        line([K1(end) K1(end)],[-50 50],'color','r','LineWidth',2);
        line([K3(1) K3(1)],[-50 50],'color','r','LineWidth',2);
        line([K3(end) K3(end)],[-50 50],'color','r','LineWidth',2);
        line([K4(1) K4(1)],[-50 50],'color','r','LineWidth',2);
        line([K4(end) K4(end)],[-50 50],'color','r','LineWidth',2);      
        legend('knee Torque','generator Torque @knee+inertia Torque @knee','generator Torque @knee','inertia Torque @knee','Friction Torque @knee');                        
        %legend('knee Torque','generator Torque @knee+inertia Torque @knee (no torque limit)','generator Torque @knee+inertia Torque @knee (torque limit-15[Nm])');                        
        title('Torque');
        ylabel('[Nm]');
        xlabel('sample no.');
        grid on;      
%         plot(10*(GenAndInertia_best_arrGR./Tk),'c','LineWidth',2); % Harvest Presentage profile
        
        
        
        figure()
        plot(Rl_best_arrGR,'LineWidth',2);
        line([K1(1) K1(1)],[-50 50],'color','r','LineWidth',2);
        line([K1(end) K1(end)],[-50 50],'color','r','LineWidth',2);
        line([K3(1) K3(1)],[-50 50],'color','r','LineWidth',2);
        line([K3(end) K3(end)],[-50 50],'color','r','LineWidth',2);
        line([K4(1) K4(1)],[-50 50],'color','r','LineWidth',2);
        line([K4(end) K4(end)],[-50 50],'color','r','LineWidth',2);      
        title('best load profile');
        ylabel('[Ohms]');
        xlabel('sample no.');
        grid on;
        
        figure()
        plot(GRs:GRe,total_eff_arr,'LineWidth',2);
        hold on;
%         plot(241,total_eff_arr(241-GRs),'or','LineWidth',3);
        plot(GRopt,total_eff_arr(1+GRopt-GRs),'or','LineWidth',3);
        title('effectivness VS Gear Ratio');
        ylabel('eff');
        xlabel('GR');
        grid on;
        
        figure()
        plot(ang,'LineWidth',2);
        line([K1(1) K1(1)],[-50 50],'color','r','LineWidth',2);
        line([K1(end) K1(end)],[-50 50],'color','r','LineWidth',2);
        line([K3(1) K3(1)],[-50 50],'color','r','LineWidth',2);
        line([K3(end) K3(end)],[-50 50],'color','r','LineWidth',2);
        line([K4(1) K4(1)],[-50 50],'color','r','LineWidth',2);
        line([K4(end) K4(end)],[-50 50],'color','r','LineWidth',2);      
        title('Ang profile');
        ylabel('[degs]');
        xlabel('sample no.');
        grid on;
        
        disp(['Total energy: ',num2str(E_totGR),'[J]']);
        disp(['Total effectivnes: ',num2str(total_effGR)]);
        disp(['Inertia effort: ',num2str(sign(inertia_wasteGR)*inertia_wasteGR),'[J]']);        
        disp(['Total human effort: ',num2str(human_effortGR),'[J]']);
        disp(['Optimum GR: ',num2str(GRopt)]);
        
        %save(strcat('f',num2str(f),'_0',num2str(HarvestPR*10),'.mat'));
        



           