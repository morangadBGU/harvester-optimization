clear all;
close all;
set(0,'DefaultFigureWindowStyle','docked');
%% Init Constants
GRs=243; GRe=243;
ii=12; %motor for test
f=1; %defines the efficancy function
C_const=0.3107/243;
HarvestPR=0.3;
Ed=0.15;
max_ig=100;
max_Eg=100;
max_torque=15;%[Nm]
Ig_supp=0.248;%[gcm^2]
R_wire=0;
weight_supp=1500-125;%weight of one system without motor [gr]
%% Load profiles
samples=101;
load('winterData.mat');
Tk=alldata(1:samples,3);% [Nm]
t=alldata(1:samples,7); %[sec]
dt=(t(end)-t(1))/length(t);
ang=alldata(1:samples,2);% [deg]
n_knee=alldata(1:samples,4);% [rpm]
alpha=-alldata(1:samples,5);% [rad/sec^2]
p=alldata(1:samples,6);% [Watt]

NEGstd=1; % std of the beginig of negative zones
NEG=find(p<0);
    ZonesInd=find(diff(NEG)-1);
    K1=NEG(1+NEGstd:ZonesInd(1));
    K2=NEG(ZonesInd(1)+NEGstd:ZonesInd(2));
    K3=NEG(ZonesInd(2)+NEGstd:ZonesInd(3));
    K4=NEG(ZonesInd(3)+NEGstd:end);
NEG=[K1;K3;K4];
POS=find(p>0);
 
% K1=5:1:13; %negative work cells of K1
% K3=57:1:73; %negative work cells of K3
% K4=84:1:98; %negative work cells of K4
% POS=[1:4,14:56,74:83,99:100]; %positive work cells

%% motors Data:
Rg=([0.413 0.978 2.77 7.41 0.367 0.807 0.248 0.524 0.323 1.24 13.5 0.527 2.3 1.22]+R_wire)./(3^0.5); % Rg - motor resistence [oham]
Km=[25.1 33.5 47.5 101 16.7 31.5 8.95 16.9 10.5 21.1 66 14 28.1 27.1]./(3^0.5); % Km - torque constant [mNm/A]
Kn=[380 285 201 95 572 303 1070 565 907 453 145 680 340 352].*(3^0.5); % Kn - speed constant [rpm/V]
Ig=([135 135 135 135 24.2 24.2 10.5 10.5 5.54 5.54 5.54 5.54 5.54 8.91]+Ig_supp); % Iner- motor inertia [g*cm2]
weight=2*([110 110 110 110 240 240 170 170 125 125 125 125 125 170]+weight_supp)*1e-3; %[Kg]
width=[12.8 12.8 12.8 12.8 36 36 26 26 48.5 48.5 48.5 48.5 48.5 48.5]*1e-3;
diameter=[45 45 45 45 40 40 40 40 22 22 22 22 22 22]*1e-3;
motor_no=[339285 251601 339286 339287 339243 339244 339241 313320 323217 323219 327739 323218 323220 311538];

%% weight Data
V=4.68; W=80; %human's weight
hip=50; %hip length
x=20; %device C.M above knee
m=weight(ii); %Total of 2 Devices weight
weight_lost=W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - (x*(W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - W*(exp((103*V)/500 + (59*m)/2000 + 59/100) - exp((103*V)/500 + 59/100))))/hip;
%Eliran interpolated eqaution
weight_lost_E=weight_lost*t(end);

%% init arrays
InertiaT=zeros(samples,1);
FrictionT=zeros(samples,1);
GenT=zeros(samples,1);
Peff=zeros(samples,1);
Ptotal=zeros(samples,1);

%% Run model
% n=[K1,K3,K4];
for GR=GRs:GRe
    InertiaT=(Ig(ii)*(10^(-7))*GR^2)*alpha(1:end); % [Nm]
    FrictionT=(C_const*GR*(pi/30))*n_knee;
    inertia_waste=trapz((pi/30)*n_knee(POS).*InertiaT(POS))*dt;
    
    T_sys=-InertiaT+FrictionT;
    GenT(NEG)=(HarvestPR*Tk(NEG)-T_sys(NEG));        
        GenT(K1(GenT(K1)<0))=0; %zeroing genrator positive work       
        GenT(K3(GenT(K3)<0))=0; %zeroing genrator positive work
        GenT(K4(GenT(K4)>0))=0; %zeroing genrator positive work
    
    
    MaxTIndexes=find((GenT+T_sys)>max_torque);
        GenT(MaxTIndexes)=max_torque-T_sys(MaxTIndexes);
    
    GenT(NEG)=GenT(NEG)/GR;    
    
    Eg=abs((n_knee(NEG)*GR)/Kn(ii));
        max_Eg_ind=find(Eg>max_Eg); Eg(max_Eg_ind)=max_Eg; % electric system Eg constrains
    ig=abs((GenT(NEG)*1000)/Km(ii));
        max_ig_ind=find(ig>max_ig); ig(max_ig_ind)=max_ig; % electric system ig constrains
    
    Ptotal(NEG)=-(Eg.*ig);
    Peff(NEG)=Ptotal(NEG) + (2*Ed*ig + ig.^2*Rg(ii));
    
    
end

%% plot

figure;
plot(GenT*GR,'k','linewidth',2)
hold on; plot(Tk,'b','linewidth',2)
plot(InertiaT,'c');
plot(-FrictionT,'g');
plot(GenT*GR+InertiaT-FrictionT,'--r','linewidth',2)

legend('Knee Torque','Gen Torque','Inertia','Friction','Total torque');
PlotNegativeZones();
grid on;

figure;
plot(p,'k','linewidth',2)
hold on; plot(Peff,'b','linewidth',2)
plot(Ptotal,'r','linewidth',2)
legend('Knee Power','P effective','P total');
PlotNegativeZones();
grid on;
