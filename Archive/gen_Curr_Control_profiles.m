clear all;
close all;
set(0,'DefaultFigureWindowStyle','docked');
%% Init Constants
GR=243;
ii=12; %motor for test
f=1; %defines the efficancy function
C_const=0.3107/243;
HarvestPRArr=[0 0.15 0.225 0.3 0.375 0.5];
Ed=0.15;
max_ig=9; %[A]
max_Eg=45; %[V]
min_Eg=max_Eg/max_ig; %[V]
max_torque=15;%[Nm]
Ig_supp=0.248;%[gcm^2]
R_wire=0;
weight_supp=1500-125;%weight of one system without motor [gr]
%% Load profiles
samples=101;
load('winterData.mat');
Tk=alldata(1:samples,3);% [Nm]
t=alldata(1:samples,7); %[sec]
dt=(t(end)-t(1))/length(t);
ang=alldata(1:samples,2);% [deg]
n_knee=alldata(1:samples,4);% [rpm]
alpha=-alldata(1:samples,5);% [rad/sec^2]
p=alldata(1:samples,6);% [Watt]

NEGstd=1; % std of the beginig of negative zones
PhazeDetectDelaySamples=2;
NEG=find(p<0);
    ZonesInd=find(diff(NEG)-1);
    K1=NEG(1+NEGstd:ZonesInd(1));
    K2=NEG(ZonesInd(1)+NEGstd:ZonesInd(2));
    K3=NEG(ZonesInd(2)+NEGstd:ZonesInd(3));
    K4=NEG(ZonesInd(3)+NEGstd:end);
NEG=[K1;K3;K4];
POS=find(p>0);
 
[val,ind1]=findpeaks(ang);
[val,ind2]=findpeaks(-ang);
PK2=ind1(1)+1:ind2(1);
PK3=ind2(1)+1:ind1(2);
PK4=ind1(2)+1:ind2(2);
PK1=[ind2(2)+1:length(ang) 1:ind1(1)]; %the +1 is due to data conflicts (K1 get the neg zone of K4)
% K1=5:1:13; %negative work cells of K1
% K3=57:1:73; %negative work cells of K3
% K4=84:1:98; %negative work cells of K4
% POS=[1:4,14:56,74:83,99:100]; %positive work cells

%% motors Data:
Rg=([0.413 0.978 2.77 7.41 0.367 0.807 0.248 0.524 0.323 1.24 13.5 0.527 2.3 1.22]+R_wire)./(3^0.5); % Rg - motor resistence [oham]
Km=[25.1 33.5 47.5 101 16.7 31.5 8.95 16.9 10.5 21.1 66 14 28.1 27.1]./(3^0.5); % Km - torque constant [mNm/A]
Kn=[380 285 201 95 572 303 1070 565 907 453 145 680 340 352].*(3^0.5); % Kn - speed constant [rpm/V]
Ig=([135 135 135 135 24.2 24.2 10.5 10.5 5.54 5.54 5.54 5.54 5.54 8.91]+Ig_supp); % Iner- motor inertia [g*cm2]
weight=2*([110 110 110 110 240 240 170 170 125 125 125 125 125 170]+weight_supp)*1e-3; %[Kg]
width=[12.8 12.8 12.8 12.8 36 36 26 26 48.5 48.5 48.5 48.5 48.5 48.5]*1e-3;
diameter=[45 45 45 45 40 40 40 40 22 22 22 22 22 22]*1e-3;
motor_no=[339285 251601 339286 339287 339243 339244 339241 313320 323217 323219 327739 323218 323220 311538];

%% weight Data
V=4.68; W=80; %human's weight
hip=50; %hip length
x=20; %device C.M above knee
m=weight(ii); %Total of 2 Devices weight
weight_lost=W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - (x*(W*(exp((11*V)/50 + (11*m)/500 + 51/100) - exp((11*V)/50 + 51/100)) - W*(exp((103*V)/500 + (59*m)/2000 + 59/100) - exp((103*V)/500 + 59/100))))/hip;
%Eliran interpolated eqaution
weight_lost_E=weight_lost*t(end);

%% init arrays
Ti=zeros(samples,1);
Tf=zeros(samples,1);
GenT=zeros(samples,1);
Peff=zeros(samples,1);
TorqueProfile=cell(1,3);
    TorqueProfile{1,1}=zeros(length(PK1),length(HarvestPRArr));
    TorqueProfile{1,2}=zeros(length(PK3),length(HarvestPRArr));
    TorqueProfile{1,3}=zeros(length(PK4),length(HarvestPRArr));
%% Run model
% n=[K1,K3,K4];
for n=2:length(HarvestPRArr) % skipping 0% harvest profile
    HarvestPR=HarvestPRArr(n);
    
    Ti=-(Ig(ii)*(10^(-7))*GR^2)*alpha(1:end); % [Nm]
    Tf=(C_const*GR*(pi/30))*n_knee;
    inertia_waste=trapz((pi/30)*n_knee(POS).*Ti(POS))*dt;
    
    T_sys=Ti+Tf;
    GenT(NEG)=HarvestPR*Tk(NEG)-T_sys(NEG); % desierd Gen torque
    
    MaxTIndexes=find((GenT+T_sys)>max_torque);
        GenT(MaxTIndexes)=max_torque-T_sys(MaxTIndexes);
    
    GenT(NEG)=GenT(NEG)/GR; % desierd Gen torque @ genrator level
    
    Eg=abs((n_knee*GR)/Kn(ii));
    Eg(POS)=0;
        min_Eg_ind=find(Eg<min_Eg); 
        Eg(min_Eg_ind)=0; % electric system Eg constrains
    ig=abs((GenT*1000)/Km(ii));
        ig(min_Eg_ind)=0; % when Eg is zero ig is zero
    max_ig_ind=find(ig>max_ig); 
        ig(max_ig_ind)=max_ig; % electric system ig constrains
       
    
    Peff=-(Eg.*ig) + (2*Ed*ig + ig.^2*Rg(ii));
    
    % Building torque profile for harvester so all PK contains data of
    % harvest torque (mainly 0 at the begining addition)
    TorqueProfile{1,1}((K1(1)-(PK1(1)-samples))+1:length(K1)+(K1(1)-(PK1(1)-samples)),n)=GenT(K1)*GR/W;
    TorqueProfile{1,2}((K3(1)-PK3(1))+1:length(K3)+(K3(1)-PK3(1)),n)=GenT(K3)*GR/W;
    TorqueProfile{1,3}((K4(1)-PK4(1))+1:length(K4)+(K4(1)-PK4(1)),n)=GenT(K4)*GR/W;       

end
for n=1:3 % adding samples in end of profile because phase detection Algo has a delay when it recognise end of PK
    TorqueProfile{1,n}=[TorqueProfile{1,n};zeros(PhazeDetectDelaySamples,length(HarvestPRArr))];
end

save('Opti_curr_2015.mat','TorqueProfile','ang','HarvestPRArr'...
    ,'PK1','PK3','PK4','K1','K3','K4');