expProfiles=0;
fitCoeffs=0;
% set(0,'DefaultFigureWindowStyle','docked');
%% Init Constants
GRs=125; GRe=512;
% GRs=243; GRe=243;
GRres=1;
if (fitCoeffs)
    load('CO_coeff.mat');
    FrictionConst = -C0(1)/243; % 0.3107/241; % from curve fit.    
    Ig_supp = -C0(3)/(243^2)*(10^7)-5.54;%[gcm^2] from curve fit.
else
    FrictionConst = 0.3107/241; % For GR=241 only
    Ig_supp=0.248;%[gcm^2]
end
GReff =@(GR) (GR>0 && GR <8)*0.96 + (GR>8 && GR <64)*0.9 + (GR>64 && GR <=512)*0.85;  % apex AM032 gear efficncy
GR_weight =@(GR) 2 * ( (GR>0 && GR <8)*156.3e-3 + (GR>8 && GR <64)*218.32e-3 + (GR>64 && GR <=512)*265.83e-3 );  % apex AM032 gear efficncy
HarvestPR=0.5;
Ed=0;%0.7;
max_ig=10; %[A]
max_Eg=45*0.6; %[V]
A_conv=9;
min_Eg=max_Eg/A_conv; %[V]
max_torque=15;%[Nm]
R_wire=0;
Brace_weight=2 * (1500-125-265.83)*1e-3;%750-125; %weight of one system without motor [gr]
W=86; %human's weight
%% Load profiles
if (expProfiles==1)
    load('Profiles\Moran_no_device_process.mat');
    samples=size(SaveData.RightK.Ang,1);
    ang=SaveData.RightK.Ang;
    Tk=SaveData.RightK.Mom;
    p=SaveData.RightK.Pow;
    t=linspace(0,SaveData.RightK.MeanCycleTime,samples);
    dt=t(2)-t(1);
else
    samples=101;
    load('winterData.mat');
    ang=alldata(1:samples,2);% [deg]
    Tk=-alldata(1:samples,3)*(W/80);% [Nm] %is written the oppsite way on the file
    p=alldata(1:samples,6);% [Watt]
    t=alldata(1:samples,7); %[sec]
    dt=(t(end)-t(1))/length(t);
end

% [n_knee,alpha]=getSmoothDerevs(ang,dt,samples);
load('derevsSave.mat');
% n_knee=n_knee*1.3;
w_knee=(pi/30)*n_knee; %[rad/sec]
    
% define gait zones
[NEG,POS,K]=getGaitZonesSampels(p,[3,4]);


%% motors Data:
Rg=([0.413 0.978 2.77 7.41 0.367 0.807 0.248 0.524 0.323 1.24 13.5 0.527 2.3 1.22]+R_wire)./(3^0.5); % Rg - motor resistence [oham]
Km=[25.1 33.5 47.5 101 16.7 31.5 8.95 16.9 10.5 21.1 66 14 28.1 27.1]./(3^0.5); % Km - torque constant [mNm/A]
Kn=[380 285 201 95 572 303 1070 565 907 453 145 680 340 352].*(3^0.5); % Kn - speed constant [rpm/V]
Ig=([135 135 135 135 24.2 24.2 10.5 10.5 5.54 5.54 5.54 5.54 5.54 8.91]+Ig_supp); % Iner- motor inertia [g*cm2]
Gen_weight=2*([110 110 110 110 240 240 170 170 125 125 125 125 125 170])*1e-3; %[Kg]
width=[12.8 12.8 12.8 12.8 36 36 26 26 48.5 48.5 48.5 48.5 48.5 48.5]*1e-3; %[m]
diameter=[45 45 45 45 40 40 40 40 22 22 22 22 22 22]*1e-3; %[m]
motor_no=[339285 251601 339286 339287 339243 339244 339241 313320 323217 323219 327739 323218 323220 311538];


%% COH
Peff.total=[];
Puser=[];
PuserW=[];
COH=[];%zeros(round((GRe-GRs)/GRres)+1,1);
COHt=[];%zeros(round((GRe-GRs)/GRres)+1,1);
OptimalChart=[];

%% weight metabolic consumption
V=4.68; 
h=50; %hip length
x=30; %device C.M above knee
d1=x; % distance from C.M to knee
d2=h-x; % distance from C.M to hip

%Eliran eqautions [watt]
f1 = @(V,m,W) (exp(0.59+0.206*V+0.059*m/2)-exp(0.59+0.206*V))*W; % knee
f2 = @(V,m,W) (exp(0.51+0.22*V+0.011*m)-exp(0.51+0.22*V))*W;      % back

Pw_func = @(V,m,W,d1,d2) ( f1(V,m,W)*d2 + f2(V,m,W)*d1 )/(d1 + d2); % interpolated equation