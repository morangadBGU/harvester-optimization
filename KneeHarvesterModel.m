Ti=(Ig(ii)*(10^(-7))*GR^2)*alpha(1:end); % [Nm]
% $$$ For friction model use this:
Tf=FrictionConst*GR*(-w_knee);
T_sys=-Ti+Tf;
GenT(NEG)=HarvestPR*Tk(NEG) - T_sys(NEG);  % desierd Gen torque
GenT(sign(GenT)==sign(w_knee))=0; % deleting parts of generator positive work
% $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

% @@@ For gear efficiency model use this:
%     T_sys=Ti;
%     GenT(NEG)=HarvestPR*Tk(NEG)*GReff(GR) - T_sys(NEG);  % desierd Gen torque
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
MaxTIndexesP=find((GenT+T_sys)>max_torque);
MaxTIndexesM=find((GenT+T_sys)<-max_torque);
GenT(MaxTIndexesP)=max_torque-T_sys(MaxTIndexesP); %cutting tourqes above system defined max torque
GenT(MaxTIndexesM)=-max_torque-T_sys(MaxTIndexesM); %cutting tourqes below system defined -max torque

GenT=GenT/GR; % desierd Gen torque @ genrator level

Eg=abs((n_knee*GR)/Kn(ii));
Eg(POS)=0;
ig=abs((GenT*1000)/Km(ii));

% % electric constrains

% % electric system Eg constrains
max_Eg_ind=find(Eg>max_Eg);
Eg(max_Eg_ind)=max_Eg; 
min_Eg_ind=find(Eg<min_Eg);
Eg(min_Eg_ind)=0; 
ig(min_Eg_ind)=0; % when Eg is zero ig is zero
max_ig_ind=find(ig>max_ig); 
ig(max_ig_ind)=max_ig; % electric system ig constrains

GenTreal=(ig * Km(ii)/1000).*sign(Tk) ; % real harvested torque

Peff.Profile=-(Eg.*ig) + (2*Ed*ig + ig.^2*Rg(ii)); % if min_Eg is not right the lost is bigger then the power
Peff.Profile(Peff.Profile>0)=0;
%figure; plot(-(Eg.*ig),'b'); hold on; plot(2*Ed*ig + ig.^2*Rg(ii),'r'); plot(Peff.Profile,'g'); legend('power','waist');
Pm=(T_sys).*(w_knee);


Device_weight=Gen_weight(ii) + GR_weight(GR) + Brace_weight;
Pw = Pw_func(V,Device_weight,W,x,h); %Total of 2 Devices weight
Peff.total = [Peff.total ;2 * trapz(Peff.Profile)*dt/t(end)]; % normalized 1 gait power [watt]

% only indexes that the harvster caused positive work to the user (Pm>0) are counted for the Ppos
Ppos = 2 * trapz(Pm(Pm>0))*dt/t(end); 
% only indexes that the harvster caused negative work to the user (Pm<0) are counted for the Pneg
Pneg = 2 * ( trapz(Pm(Pm<0)) + trapz((GenTreal*GR).*w_knee) )*dt/t(end) ; 
% this is the energy that we saved from the user

PuserW=[PuserW ; Pw + (4*Ppos+Pneg)];
Puser=[Puser ;  (4*Ppos+Pneg)];