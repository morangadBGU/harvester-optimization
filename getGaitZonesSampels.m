function [ NEGforHarv,POS,NegZones ] = getGaitZonesSampels( p , DesierdNegforHarvest)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
NEGforHarv=[]; POS=[];
NEGstd=1; % std of the beginig of negative zones
NEG=find(p<0);
ZonesInd=find(diff(NEG)-1);
NegZones=cell(4,1);
NegZones{1}=NEG(1+NEGstd:ZonesInd(1));
NegZones{2}=NEG(ZonesInd(1)+NEGstd:ZonesInd(2));
NegZones{3}=NEG(ZonesInd(2)+NEGstd:ZonesInd(3));
NegZones{4}=NEG(ZonesInd(3)+NEGstd:end);
for n=DesierdNegforHarvest
    NEGforHarv=[ NEGforHarv ; NegZones{n} ]; % Negative zones to be harvest.
end
% adding the negative zones that will not harvest to the positive 
%so the positive waste will be calculate in those samples as well
for n=1:4     
    if (isempty(find(DesierdNegforHarvest==n,1)))
        POS=[POS; (NegZones{n}(1) - NEGstd :  NegZones{n}(end))'];
    end
end
POS=[POS ; find(p >= 0)];
POS=sort(POS);