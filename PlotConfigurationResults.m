figure;
plot(Tk,'b','linewidth',4)
hold on;% plot(GenT*GR,'--k','linewidth',2)
plot(GenTreal*GR,'k','linewidth',3)
plot(Ti,'c','linewidth',3);
plot(Tf,'g','linewidth',3);
plot(GenTreal*GR+T_sys,'r','linewidth',4)
l=legend('T_h', 'T_g', 'T_I', 'T_f', 'T_{sys}', 'Interpreter', 'LaTex');
set(l,'fontsize',14); set(l, 'position',[0.7040    0.7158    0.0474    0.1889]);
xlabel('% of Gait','FontSize',14); ylabel('Torque [Nm]','FontSize',14);
PlotNegativeZones();
axis([0 101 -50 35]);

title(['Optimal Torque Profile at: ',num2str(HarvestPR*100),...
    '% harvest. Genrator no.: ',num2str(motor_no(ii))],'FontSize',14);%,' G.R: ',num2str())

figure;
plot(p,'k','linewidth',2)
hold on; plot(Peff.Profile,'b','linewidth',2)
legend('Normal Gait Knee Power Profile','Effective Harvested Power');
PlotNegativeZones();
axis([0 101 -100 60]);
grid on;
title(['Optimal Power Profile at: ',num2str(HarvestPR*100),...
    '% harvest. Genrator no.: ',num2str(motor_no(ii))]);