figure;
h3=subplot(2,1,1); hold(h3,'on');
plot(h3,GRs:GRres:GRe,COHt,'k','linewidth',3); 
xlabel(h3,'Gear Ratio','FontSize',14,'Interpreter','LaTex');
ylabel('COH total','FontSize',14,'Interpreter','LaTex'); %ylim(h3,[0 25]);
% ylim(h3,[2 8]);
title1STR=cell(2,1); 
title1STR{1}=['COHt over Gear Ratio for: ',num2str(HarvestPR*100),...
    '% harvest. Genrator no. : ',num2str(motor_no(ii))];
title1STR{2}=['Optimal Gear Ratio: ',num2str(Opti.GR)];   
title(h3,title1STR,'FontSize',14);
plot(h3,Opti.GR,Opti.COHt,'.r','markersize',25);
axes(h3);
text(Opti.GR-25,Opti.COHt+0.35,['COHt = ',num2str(round(Opti.COHt*100)/100)],'FontSize',12,'Interpreter','LaTex');

h4=subplot(2,1,2); hold(h4,'on');

[ax2,h41,h42]=plotyy(h4,GRs:GRres:GRe,PuserW,GRs:GRres:GRe,-Peff.total); 
hold(ax2(1), 'on'); hold(ax2(2), 'on');
set(h41,'linewidth',3);
set(h42,'linewidth',3);
ylabel(ax2(1),'$\Delta P_{meta} [W]$','FontSize',14,'Interpreter','LaTex'); 
ylabel(ax2(2),'$P_{eff} [W]$','FontSize',14,'Interpreter','LaTex');
xlabel(h4,'Gear Ratio','FontSize',14,'Interpreter','LaTex');

plot(ax2(1),Opti.GR, Opti.Meta,'.r','markersize',25);
axes(ax2(1));
text(Opti.GR-35,Opti.Meta-1,['$\Delta P_{meta} [W]$ = ',num2str(round(Opti.Meta*100)/100)],'FontSize',12,'Interpreter','LaTex');
plot(ax2(2),Opti.GR, -Opti.HarvPower,'.r','markersize',25);
axes(ax2(2));
text(Opti.GR-35,-Opti.HarvPower -0.5,['$P_{eff} [W]$ = ',num2str(round(-Opti.HarvPower*100)/100)],'FontSize',12,'Interpreter','LaTex');




title2STR=cell(2,1); 
title2STR{1}=['Metabolic and harvested power over Gear Ratio for: ',num2str(HarvestPR*100),...
    '% harvest. Genrator no.: ',num2str(motor_no(ii))];
title2STR{2}=['Metabolic Consumption: ',num2str(Opti.Meta),...
    ' Total Harvested Power: ',num2str(-Opti.HarvPower)];

title(h4,title2STR,'FontSize',14);


linkaxes([h3,ax2(1),ax2(2)],'x');
