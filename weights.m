clear all; close all; 
m=0.5:0.1:5;
W=80;
V=4.68; 
f1=(exp(0.59+0.206*V+0.059*m/2)-exp(0.59+0.206*V))*W;
f2=(exp(0.51+0.22*V+0.011*m)-exp(0.51+0.22*V))*W;
x=30; h=50;
d1=x; % distance from C.M to knee
d2=h-x; % distance from C.M to hip

 f = ( f1*d2 + f2*d1 )/(d1 + d2); % interpolated equation

 plot(m,f1,'LineWidth',2);
hold on;
plot(m,f2,'r','LineWidth',2);
hold on;
plot(m,f,'g','LineWidth',2);

xlabel('Load mass [Kg]');
ylabel('Estimated additional metabolic consumption[Watt]');
% legend('knee load power waste','waist load power waste','interpolated function');
legend({'$\Delta P_{mass}^{knee}$','$\Delta P_{mass}^{back}$','$\Delta P_{mass}$'},...
    'Fontsize',14,'Interpreter','LaTex');
grid on;

% syms m V W hip x;
% f1=(exp(0.59+0.206*V+0.059*m/2)-exp(0.59+0.206*V))*W;
% f2=(exp(0.51+0.22*V+0.011*m*2)-exp(0.51+0.22*V))*W;
% % f=(f1-f2)*(x/hip)+f2;
% f = ( f1(V,m,W)*d2 + f2(V,m,W)*d1 )/(d1 + d2); % interpolated equation

% m=0.5:0.1:5;
% W=80;
% V=4.68; x=20; hip=50;
% f=subs(f);
% plot(m,f,'--k','LineWidth',2);