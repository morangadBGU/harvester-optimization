clear all; close all;
initOpti();
initComparison();
%% Run model

for HarvestPR=[0 0.15 0.225 0.3 0.375 0.5]
    UpdateConfiguration();
    for GR=GRs:GRres:GRe
        KneeHarvesterModel();       
%                 PlotConfigurationResults();
    end
    COH=Puser./(Peff.total);
    COHt=PuserW./(Peff.total);
    FindOptimal();
%         PlotOptiResults();
    OptimalChart=[OptimalChart; motor_no(ii), Opti.GR, Opti.COHt, Opti.HarvPower, Opti.Meta, HarvestPR, Ppos, Pneg];    

end

PlotExpCompare();
