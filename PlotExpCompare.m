close all;
figure('Name', 'Metabolic Consumption Vs. Scaler'); 
h5=axes; hold(h5,'on');
pBar1 = bar(h5,[-0.15; 0; ExpRes.Scaler],[[ExpRes.MetaAVG(7); ExpRes.MetaAVG(1:6)],[Pw_func(V,3,W,d1,d2); OptimalChart(1:end, 5)]],'group');
set(pBar1(2),'facecolor',[0.8 0.6 0.1]) 
% p=errorbar(h5,ExpRes.Scaler-1/100,ExpRes.MetaAVG,ExpRes.MetaSTD,'.k');
% set(p,'linewidth',3); 
% set(p,'color',[0.8 0.6 0.1]);
set(h5,'XTick',[-0.15; 0; ExpRes.Scaler]','fontsize',14)
set(h5,'XTickLabel',{'Weight Only','0', '0.15', '0.225', '0.3', '0.375', '0.5'},'fontsize',14)

legend('Experiment','Model');
xlabel('Torque-Ratio','FontSize',14,'Interpreter','LaTex');
ylabel('$\Delta P_{meta}[W]$','FontSize',14,'Interpreter','LaTex');
% ylim(h5,[0 max(ExpRes.MetaAVG+ExpRes.MetaSTD)*1.1]);

figure('Name', 'Effective Harvest Power Consumption Vs. Scaler'); 
h6=axes; hold(h6,'on');
pBar2 = bar(h6,ExpRes.Scaler,[ExpRes.PowerAVG, -OptimalChart(2:end,4)],'group');
set(pBar2(2),'facecolor',[0.8 0.6 0.1]) 
p=errorbar(h6,ExpRes.Scaler-1/100,ExpRes.PowerAVG,ExpRes.PowerSTD,'.k');
set(p,'linewidth',3); 
set(p,'color',[0.8 0.2 0.2]);
ax = gca;
set(ax,'XTick',ExpRes.Scaler','fontsize',14)
legend('Experiment','Model');
xlabel('Torque-Ratio','FontSize',14,'Interpreter','LaTex');
ylabel('$ P_{eff}[W]$','FontSize',14,'Interpreter','LaTex');


figure('Name','COHt Vs Scaler'); h7=axes; hold(h7,'on');
fitresult=SplineFit_COH( ExpRes.Scaler, ExpRes.MetaAVG(2:6) ./ ExpRes.PowerAVG );
% plot( ExpRes.Scaler, ExpRes.MetaAVG(2:6) ./ ExpRes.PowerAVG ,'.k');
% Plot fit with data.
h = plot(fitresult); 
	set(h,'color',[0 0.5 0.5]); set(h,'linewidth',4);
plot(h7, ExpRes.Scaler, ExpRes.MetaAVG(2:6) ./ ExpRes.PowerAVG,'.k','markersize',30);
legend('fitted spline','Experiment''s results')
xlim(h7,[ExpRes.Scaler(1)-0.05 ExpRes.Scaler(end)+0.05]);
ax = gca;
set(ax,'XTick',ExpRes.Scaler','fontsize',14)
xlabel('Torque-Ratio','FontSize',14,'Interpreter','LaTex');
ylabel('$ COHt $','FontSize',14,'Interpreter','LaTex');
% title(h5,'$\Delta P_{meta}[W]$','FontSize',14,'Interpreter','LaTex');

figure('Name', 'Weight effect on the metabolic power addition'); h8=axes; hold(h8,'on');
hold(h8, 'on'); 
title(h8,' Device weight effect on the additional metabolic consumption','FontSize',14,'Interpreter','LaTex');
ModelWeightMeta = Pw_func(V,3,W,d1,d2);
pBar3 = bar(h8, 1, ExpRes.MetaAVG(7), 0.4);
pBar4 = bar(h8, 1.5, ModelWeightMeta, 0.4); 
set(pBar4,'facecolor',[0.8 0.6 0.1]) 
h8.XTick=[];
legend(h8, 'Experiment','Model');
ylabel(h8, '$\Delta P_{meta}[W]$','FontSize',14,'Interpreter','LaTex');

disp(['Wight Metabolic error: ',num2str(abs(ModelWeightMeta-ExpRes.MetaAVG(7))/ExpRes.MetaAVG(7) * 100),'%' ]);

