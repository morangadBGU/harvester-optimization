T_total=GenTreal*GR+T_sys;
fs=figure;
fs.Position=[962   562   958   434];
plot(Tk,'b','linewidth',4)
hold on;% plot(GenT*GR,'--k','linewidth',2)
plot(T_total,'r','linewidth',4)
l=legend({'$T_{normal-gait}$'; '$T_{harvest}$'}, 'Interpreter', 'LaTex');
set(l, 'FontSize',32);
set(l,'fontsize',14); set(l, 'position',[0.7345    0.1734    0.1501    0.1171]);
xlabel('% of Gait','FontSize',14); ylabel('Torque [Nm]','FontSize',14);
PlotNegativeZones();
axis([0 101 -50 35]);

K1up=K{1}; 
K1dn=K{1}(end:-1:1);
ff1 = fill([K1up; K1dn],[Tk(K1up); T_total(K1dn)],'g');
set(ff1,'facealpha', 0.3);
K3up=K{3}; 
K3dn=K{3}(end:-1:1);
ff3 = fill([K3up; K3dn],[Tk(K3up); T_total(K3dn)],'g');
set(ff3,'facealpha', 0.3);
K4up=K{4}; 
K4dn=K{4}(end:-1:1);
ff4 = fill([K4up; K4dn],[Tk(K4up); T_total(K4dn)],'g');
set(ff4,'facealpha', 0.3);

K1mid=(K{1}(1) + K{1}(end))/2;
K3mid=(K{3}(1) + K{3}(end))/2;
K4mid=(K{4}(1) + K{4}(end))/2;
text(K1mid-1,-20,'K1', 'fontsize',14)
text(K3mid-1,-20,'K3', 'fontsize',14)
text(K4mid-1,-20,'K4', 'fontsize',14)

