clear all; %close all;
initOpti();

%% Run model
for ii = 12
    
    UpdateConfiguration();
    for GR=243%GRs:GRres:GRe
        KneeHarvesterModel();        
        PlotConfigurationResults();
    end
    COH=Puser./(-Peff.total);
    COHt=PuserW./(-Peff.total);
    FindOptimal();
%         PlotOptiResults();
    OptimalChart=[OptimalChart; motor_no(ii), Opti.GR, Opti.COHt, Opti.HarvPower, Opti.Meta, HarvestPR];
    
    
end
